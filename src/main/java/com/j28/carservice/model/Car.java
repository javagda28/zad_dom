package com.j28.carservice.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Manufacturer manufacturer;
    private String registrationNumber;
    private String owner;
    private int yearProduced;
    private int mileage;

    public Car(Manufacturer manufacturer, String registrationNumber, String owner, int yearProduced, int mileage) {
        this.manufacturer = manufacturer;
        this.registrationNumber = registrationNumber;
        this.owner = owner;
        this.yearProduced = yearProduced;
        this.mileage = mileage;
    }

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER)
    private Set<ServiceRequest> serviceRequests;
}
