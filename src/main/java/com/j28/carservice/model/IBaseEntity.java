package com.j28.carservice.model;

public interface IBaseEntity {
    Long getId();
}
