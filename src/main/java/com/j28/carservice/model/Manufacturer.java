package com.j28.carservice.model;

public enum Manufacturer {
    BMW,
    AUDI,
    HONDA,
    MAZDA,
    FORD
}
