package com.j28.carservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceRequest implements IBaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    private LocalDateTime dateAdded;

    private LocalDateTime dateDone;
    private String content;
    private boolean urgent;

    @ManyToOne()
    private Car car;


    public ServiceRequest(String content, boolean urgent) {
        this.content = content;
        this.urgent = urgent;
    }
}
