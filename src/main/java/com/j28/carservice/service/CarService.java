package com.j28.carservice.service;


import com.j28.carservice.database.EntityDao;
import com.j28.carservice.model.Car;
import com.j28.carservice.model.Manufacturer;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public class CarService {
    private EntityDao dao = new EntityDao();

    public void save(Car c){
        dao.saveOrUpdate(c);
    }

    public List<Car> listAll(){
        return dao.getAll(Car.class);
    }

    public void removeCar(Long id) {
        dao.delete(Car.class, id);
    }

    public Optional<Car> findById(Long identifier) {
        return dao.getById(Car.class, identifier);
    }

    public void update(Long identifier, HttpServletRequest req) {
        Optional<Car> carOptional = findById(identifier);
        if(carOptional.isPresent()){
            Car car = carOptional.get();

            car.setManufacturer(Manufacturer.valueOf(req.getParameter("manufacturer")));
            car.setRegistrationNumber(req.getParameter("registrationNumber"));
            car.setOwner(req.getParameter("owner"));
            car.setYearProduced(Integer.parseInt(req.getParameter("yearProduced")));
            car.setMileage(Integer.parseInt(req.getParameter("mileage")));

            save(car);
        }
    }
}
