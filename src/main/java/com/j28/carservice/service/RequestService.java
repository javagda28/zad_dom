package com.j28.carservice.service;

import com.j28.carservice.database.EntityDao;
import com.j28.carservice.model.ServiceRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public class RequestService {
    private final EntityDao dao = new EntityDao();
    public void save(ServiceRequest request) {
        dao.saveOrUpdate(request);
    }

    public List<ServiceRequest> listAll() {
        return dao.getAll(ServiceRequest.class);
    }

    public Optional<ServiceRequest> findById(Long id) {
        return dao.getById(ServiceRequest.class, id);
    }

    public void remove(Long id) {
        dao.delete(ServiceRequest.class, id);
    }

    public ServiceRequest update(Long identifier, HttpServletRequest req) {

            Optional<ServiceRequest> serviceRequestOptional = findById(identifier);
            if(serviceRequestOptional.isPresent()){
                ServiceRequest serviceRequest = serviceRequestOptional.get();

                serviceRequest.setContent(req.getParameter("content"));
                serviceRequest.setUrgent(req.getParameter("urgent")!= null && req.getParameter("urgent").equals("on"));

                save(serviceRequest);
                return serviceRequest;
            }
        throw new UnsupportedOperationException();
    }
}
