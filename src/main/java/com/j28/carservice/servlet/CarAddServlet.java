package com.j28.carservice.servlet;

import com.j28.carservice.model.Car;
import com.j28.carservice.model.Manufacturer;
import com.j28.carservice.service.CarService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/car/add")
public class CarAddServlet extends HttpServlet {
    private final CarService carService = new CarService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("manufacturers", Manufacturer.values());
        req.getRequestDispatcher("/car-form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Car car = createCarFromParameters(req);
        carService.save(car);

        resp.sendRedirect("/car/list");
    }

    private Car createCarFromParameters(HttpServletRequest req) {
        return new Car(
                Manufacturer.valueOf(req.getParameter("manufacturer")),
                req.getParameter("registrationNumber"),
                req.getParameter("owner"),
                Integer.parseInt(req.getParameter("yearProduced")),
                Integer.parseInt(req.getParameter("mileage")));
    }
}
