package com.j28.carservice.servlet;

import com.j28.carservice.model.Car;
import com.j28.carservice.model.Manufacturer;
import com.j28.carservice.service.CarService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/car/edit")
public class CarEditServlet extends HttpServlet {
    private final CarService carService = new CarService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("carId"));

        Optional<Car> carOptional = carService.findById(identifier);
        if(carOptional.isPresent()) {
            req.setAttribute("car", carOptional.get());

            req.setAttribute("manufacturers", Manufacturer.values());

            req.getRequestDispatcher("/car-form.jsp").forward(req, resp);
        }else{
            resp.sendRedirect("/car/list");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("carId"));

        carService.update(identifier, req);

        resp.sendRedirect("/car/list");
    }
}
