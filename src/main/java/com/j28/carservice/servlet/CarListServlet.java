package com.j28.carservice.servlet;

import com.j28.carservice.service.CarService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/car/list")
public class CarListServlet extends HttpServlet {
    private final CarService carService= new CarService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("cars", carService.listAll());
        req.getRequestDispatcher("/car-list.jsp").forward(req, resp);
    }
}
