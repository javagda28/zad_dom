package com.j28.carservice.servlet;

import com.j28.carservice.service.CarService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/car/remove")
public class CarRemoveServlet extends HttpServlet {
    private final CarService carService = new CarService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // pobieram od użytkownika parametr (indeks)
        Long id = Long.valueOf(req.getParameter("carId"));

        // usuwam cara z listy
        carService.removeCar(id);

        // przekierowuje użytkownika na stronę z listą Carów
        resp.sendRedirect("/car/list");
    }
}
