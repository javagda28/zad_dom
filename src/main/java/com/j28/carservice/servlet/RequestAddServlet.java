package com.j28.carservice.servlet;


import com.j28.carservice.model.Car;
import com.j28.carservice.model.Manufacturer;
import com.j28.carservice.model.ServiceRequest;
import com.j28.carservice.service.CarService;
import com.j28.carservice.service.RequestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/request/add")
public class RequestAddServlet extends HttpServlet {
    private final RequestService requestService = new RequestService();
    private final CarService carService = new CarService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long carId = Long.parseLong(req.getParameter("carId"));
        req.setAttribute("carIdentifier", carId);
        req.getRequestDispatcher("/request-form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServiceRequest request = createRequestFromParameters(req);
        Optional<Car> optionalCar = carService.findById(Long.valueOf(req.getParameter("carId")));
        if(optionalCar.isPresent()){
            Car car = optionalCar.get();

            request.setCar(car);
            requestService.save(request);

            resp.sendRedirect("/request/list?carId="+car.getId());
        }else{
            resp.sendRedirect("/car/list");
        }
    }

    private ServiceRequest createRequestFromParameters(HttpServletRequest req) {
        return new ServiceRequest(
                req.getParameter("content"),
                req.getParameter("urgent")!= null && req.getParameter("urgent").equals("on"));
    }
}
