package com.j28.carservice.servlet;

import com.j28.carservice.model.ServiceRequest;
import com.j28.carservice.service.RequestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/request/edit")
public class RequestEditServlet extends HttpServlet {
    private final RequestService serviceRequestService = new RequestService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("serviceRequestId"));

        Optional<ServiceRequest> serviceRequestOptional = serviceRequestService.findById(identifier);
        if(serviceRequestOptional.isPresent()) {
            req.setAttribute("request", serviceRequestOptional.get());

            req.getRequestDispatcher("/request-form.jsp").forward(req, resp);
        }else{
            resp.sendRedirect("/car/list");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("serviceRequestId"));

        ServiceRequest serviceRequest = serviceRequestService.update(identifier, req);

        resp.sendRedirect("/request/list?carId="+serviceRequest.getCar().getId());
    }
}
