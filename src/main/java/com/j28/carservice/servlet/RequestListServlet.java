package com.j28.carservice.servlet;

import com.j28.carservice.model.Car;
import com.j28.carservice.service.CarService;
import com.j28.carservice.service.RequestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/request/list")
public class RequestListServlet extends HttpServlet {
    private final RequestService requestService= new RequestService();
    private final CarService carService= new CarService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Car> optionalCar = carService.findById(Long.valueOf(req.getParameter("carId")));
        if(optionalCar.isPresent()) {
            Car car = optionalCar.get();

            req.setAttribute("requests", car.getServiceRequests());
            req.getRequestDispatcher("/request-list.jsp").forward(req, resp);
        }else{
            resp.sendRedirect(req.getHeader("referer"));
        }
    }
}
