package com.j28.carservice.servlet;

import com.j28.carservice.model.ServiceRequest;
import com.j28.carservice.service.RequestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Service;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

@WebServlet("/request/setDone")
public class RequestMarkDoneServlet extends HttpServlet {
    private final RequestService requestService= new RequestService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("serviceRequestId"));
        Optional<ServiceRequest> requestServiceOptional =  requestService.findById(id);
        if(requestServiceOptional.isPresent()){
            ServiceRequest request = requestServiceOptional.get();
            request.setDateDone(LocalDateTime.now());

            requestService.save(request);
        }
        resp.sendRedirect(req.getHeader("referer"));
    }
}
