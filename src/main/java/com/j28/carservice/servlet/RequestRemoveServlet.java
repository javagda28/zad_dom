package com.j28.carservice.servlet;

import com.j28.carservice.service.RequestService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/request/remove")
public class RequestRemoveServlet extends HttpServlet {
    private final RequestService serviceRequestService = new RequestService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // pobieram od użytkownika parametr (indeks)
        Long id = Long.valueOf(req.getParameter("serviceRequestId"));

        // usuwam serviceRequesta z listy
        serviceRequestService.remove(id);

        // przekierowuje użytkownika na stronę z listą Carów
        resp.sendRedirect(req.getHeader("referer"));
    }
}
