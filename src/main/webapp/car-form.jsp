<%@ page import="com.j28.carservice.model.Car" %><%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/30/19
  Time: 8:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Car Form</title>
</head>
<body>
<h1>Index page</h1>
<jsp:include page="navigator.jsp"/>
<%
    Car car = null;
    if(request.getAttribute("car")!=null){
        car = (Car) request.getAttribute("car");
    }
%>
<form action="<%= car != null ? "/car/edit" : "/car/add" %>" method="post">
    <input type="hidden" name="carId" value="<%=car != null ? car.getId() : ""%>">
    Manufacturer: <select name="manufacturer">
    <c:forEach var="manufacturer" items="${requestScope.manufacturers}" varStatus="loop">
        <option value="${manufacturer}" ${manufacturer == requestScope.car.getManufacturer() ? "selected" : ""}>${manufacturer}</option>
    </c:forEach>
    </select> <br/>
    Registration number: <input type="text" name="registrationNumber" value="<%=car != null ? car.getRegistrationNumber() : ""%>"> <br/>
    Owner: <input type="text" name="owner" value="<%=car != null ? car.getOwner() : ""%>"> <br/>
    Year produced: <input type="number" min="1990" max="2019" name="yearProduced" value="<%=car != null ? car.getYearProduced() : ""%>"> <br/>
    Mileage:<input type="number" name="mileage" value="<%=car != null ? car.getMileage() : ""%>"> <br/><br/>

    <input type="submit">
</form>
</body>
</html>
