<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/23/19
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Car List</title>
    <style>
        td{
            border: 1px solid #000;
        }
    </style>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Car List</h1>
<table style="border: 1px solid #000; width: 100%">
    <tr>
        <th>Lp.</th>
        <th>Id</th>
        <th>Manufacturer</th>
        <th>Registration number</th>
        <th>Owner</th>
        <th>Year produced</th>
        <th>Mileage</th>
        <th></th>
    </tr>
<c:forEach var="car" items="${requestScope.cars}" varStatus="loop">
    <tr>
        <td>${loop.index}</td>
        <td>${car.getId()}</td>
        <td>${car.getManufacturer()}</td>
        <td>${car.getRegistrationNumber()}</td>
        <td>${car.getOwner()}</td>
        <td>${car.getYearProduced()}</td>
        <td>${car.getMileage()}</td>
        <td>
            <a href="/car/remove?carId=${car.getId()}">Usun</a>
        </td>
        <td>
            <a href="/car/edit?carId=${car.getId()}">Edit</a>
        </td>
        <td>
            <a href="/request/add?carId=${car.getId()}">Add request</a>
        </td>
        <td>
            <a href="/request/list?carId=${car.getId()}">List requests</a>
        </td>
    </tr>
</c:forEach>

</table>
</body>
</html>
