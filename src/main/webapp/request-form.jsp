<%@ page import="com.j28.carservice.model.ServiceRequest" %><%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/30/19
  Time: 8:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>ServiceRequest Form</title>
</head>
<body>
<h1>Index page</h1>
<jsp:include page="navigator.jsp"/>
<%
    ServiceRequest serviceRequest = null;
    if(request.getAttribute("request")!=null){
        serviceRequest = (ServiceRequest) request.getAttribute("request");
    }
%>
<form action="<%= serviceRequest != null ? "/request/edit" : "/request/add" %>" method="post">
    <input type="hidden" name="serviceRequestId" value="<%=serviceRequest != null ? serviceRequest.getId() : ""%>">
    <input type="hidden" name="carId" value="${requestScope.carIdentifier}">
    Content: <input type="text" name="content" value="<%=serviceRequest != null ? serviceRequest.getContent() : ""%>"> <br/>
    Urgent: <input type="checkbox" name="urgent" <%= serviceRequest != null && serviceRequest.isUrgent() ? "checked" : ""%>> <br/>

    <input type="submit">
</form>
</body>
</html>
