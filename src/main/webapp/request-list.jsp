<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/23/19
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>

<%@ page isELIgnored="false" %>
<html>
<head>
    <title>ServiceRequest List</title>
    <style>
        td{
            border: 1px solid #000;
        }
    </style>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>ServiceRequest List</h1>
<table style="border: 1px solid #000; width: 100%">
    <tr>
        <th>Lp.</th>
        <th>Id</th>
        <th>Date created</th>
        <th>Date done</th>
        <th>Content</th>
        <th>Urgent</th>
        <th></th>
    </tr>
<c:forEach var="serviceRequest" items="${requestScope.requests}" varStatus="loop">
    <tr>
        <td>${loop.index}</td>
        <td>${serviceRequest.getId()}</td>
        <td>${serviceRequest.getDateAdded()}</td>
        <td>${serviceRequest.getDateDone()}</td>
        <td>${serviceRequest.getContent()}</td>
        <td>${serviceRequest.isUrgent()}</td>
        <td>
            <a href="/request/remove?serviceRequestId=${serviceRequest.getId()}">Usun</a>
        </td>
        <td>
            <a href="/request/edit?serviceRequestId=${serviceRequest.getId()}">Edit</a>
        </td>
        <c:if test="${serviceRequest.getDateDone() == null}">
        <td>
            <a href="/request/setDone?serviceRequestId=${serviceRequest.getId()}">Mark done</a>
        </td>
        </c:if>
    </tr>
</c:forEach>

</table>
</body>
</html>
